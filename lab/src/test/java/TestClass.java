import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestClass {

    /**
     * Write your tests here.
     * Don't forget to put @Test before your test method.
     * */
     @Test
    public void test1() throws IOException {
        Adder a = new Adder(2,3);
        assertEquals(5, a.add());
    }

    @Test
    public void test2() throws IOException {
        Adder a = new Adder(4,3);
        assertEquals(1, a.sub());
    }

    @Test
    public void test3() throws IOException {
        Multiplier m = new Multiplier(2,3);
        assertEquals(6, m.mul());
    }

    @Test
    public void test4() throws IOException {
        Multiplier m = new Multiplier(2,3);
        assertEquals(8, m.pow(3));
    }

}
